package com.example.demo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.http.dsl.Http;
import org.springframework.integration.scripting.dsl.Scripts;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//---------------------------------
// Rest interaction Basic
//---------------------------------

interface RentalService {
    Object findPlants(String name, LocalDate startDate, LocalDate endDate);
}

@Service
class RentalServiceImpl implements RentalService {
    RestTemplate restTemplate = new RestTemplate();
    @Override
    public String findPlants(String name, LocalDate startDate, LocalDate endDate) {
        return restTemplate.getForObject("http://localhost:8088/api/v1/plant", String.class);
    }
}

//---------------------------------
// Integration interaction basic
//---------------------------------

//@MessagingGateway
//interface RentalService {
//
//    @Gateway(requestChannel = "req-channel", replyChannel = "rep-channel")
//    Object findPlants(@Payload String name, @Header("startDate") LocalDate startDate, @Header("endDate") LocalDate endDate);
//}

//@Service
//class RentalServiceImpl {
//    RestTemplate restTemplate = new RestTemplate();
//    public String findPlants(Message<?> message) {
//        return restTemplate.getForObject("http://localhost:8088/api/v1/plant", String.class);
//    }
//}
//@Configuration
//class Flows {
//    @Bean
//    IntegrationFlow rentMTFlow() {
//        return IntegrationFlows
//                .from("req-channel")
//                .handle("rentalServiceImpl","findPlants")
//                .channel("rep-channel")
//                .get();
//    }
//}
// OutboundGateway (Adapter)
//@Configuration
//class Flows {
//    @Bean
//    IntegrationFlow rentMTFlow() {
//        return IntegrationFlows
//                .from("req-channel")
//                .handle(Http.outboundGateway("http://localhost:8088/api/v1/plant?filter[plant]=name==*{name}*")
//                        .uriVariable("name", "payload")
//                        .httpMethod(HttpMethod.GET)
//                        .expectedResponseType(String.class))
//                .channel("rep-channel")
//                .get();
//    }
//}

//---------------------------------
// JSON transformation
//---------------------------------

// Common
//@Data
//class Plant {
//    String _id;
//    String name;
//    String description;
//    Integer price;
//}

// Json to Java Object
//@Configuration
//class Flows {
//    @Bean
//    IntegrationFlow rentMTFlow() {
//        return IntegrationFlows
//                .from("req-channel")
//                .handle(Http.outboundGateway("http://localhost:8088/api/v1/plant?filter[plant]=name==*{name}*")
//                        .uriVariable("name", "payload")
//                        .httpMethod(HttpMethod.GET)
//                        .expectedResponseType(String.class))
//                .transform(Scripts.processor("classpath:/JsonApi2HAL.js")
//                        .lang("javascript"))
//                .transform(Transformers.fromJson(Plant[].class))
//                .channel("rep-channel")
//                .get();
//    }
//}
// Json to HAL
//@Service
//class CustomTransformer {
//    @Autowired
//    ObjectMapper mapper;
//
//    public CollectionModel<EntityModel<Plant>> fromJson(String json) {
//        try {
//            List<Plant> plants = mapper.readValue(json, new TypeReference<List<Plant>>() {});
//            return new CollectionModel<>(plants.stream().map(p -> new EntityModel<>(p, new Link("http://localhost:8088/api/v1/plant/" + p._id))).collect(Collectors.toList()));
//        } catch (IOException e) {
//            return null;
//        }
//    }
//}

//@Configuration
//class Flows {
//    @Bean
//    IntegrationFlow rentMTFlow() {
//        return IntegrationFlows
//                .from("req-channel")
//                .handle(Http.outboundGateway("http://localhost:8088/api/v1/plant?filter[plant]=name==*{name}*")
//                        .uriVariable("name", "payload")
//                        .httpMethod(HttpMethod.GET)
//                        .expectedResponseType(String.class))
//                .transform(Scripts.processor("classpath:/JsonApi2HAL.js")
//                        .lang("javascript"))
//                .handle("customTransformer", "fromJson")
//                .channel("rep-channel")
//                .get();
//    }
//}

//---------------------------------
// Scatter and gather
//---------------------------------

//@Service
//class CustomTransformer {
//    @Autowired
//    ObjectMapper mapper;
//
//    public CollectionModel<EntityModel<Plant>> fromJson(String json) {
//        try {
//            List<Plant> plants = mapper.readValue(json, new TypeReference<List<Plant>>() {});
//            return new CollectionModel<>(plants.stream().map(p -> new EntityModel<>(p, new Link("http://localhost:8088/api/v1/plant/" + p._id))).collect(Collectors.toList()));
//        } catch (IOException e) {
//            return null;
//        }
//    }
//
//    public CollectionModel<EntityModel<Plant>> fromHALForms(String json) {
//        try {
//            List<Plant> plants = mapper.readValue(json, new TypeReference<List<Plant>>() {});
//            return new CollectionModel<>(plants.stream().map(p -> new EntityModel<>(p, new Link("http://localhost:8090/" + p._id))).collect(Collectors.toList()));
//        } catch (IOException e) {
//            return null;
//        }
//    }
////    public CollectionModel<EntityModel<Plant>> fromHALForms(String json) {
////        try {
////            return mapper.readValue(json, new TypeReference<CollectionModel<EntityModel<Plant>>>() {});
////        } catch (IOException e) {
////            return null;
////        }
////    }
//}


//@Configuration
//class Flows {
//    @Bean
//    IntegrationFlow scatterComponent() {
//        return IntegrationFlows.from("req-channel")
//                .publishSubscribeChannel(conf ->
//                        conf.applySequence(true)
//                                .subscribe(f -> f.channel("rentmt-req"))
//                                .subscribe(f -> f.channel("rentit-req"))
//                )
//                .get();
//    }
//
//    @Bean
//    IntegrationFlow rentMTFlow() {
//        return IntegrationFlows
//                .from("rentmt-req")
//                .handle(Http.outboundGateway("http://localhost:8088/api/v1/plant?filter[plant]=name==*{name}*")
//                        .uriVariable("name", "payload")
//                        .httpMethod(HttpMethod.GET)
//                        .expectedResponseType(String.class))
//                .transform(Scripts.processor("classpath:/JsonApi2HAL.js")
//                        .lang("javascript"))
//                .handle("customTransformer", "fromJson")
//                .channel("gather-channel")
//                .get();
//    }
//
//    @Bean
//    IntegrationFlow rentItFlow() {
//        return IntegrationFlows.from("rentit-req")
//                .handle(Http.outboundGateway("http://localhost:8090/api/sales/plants?name={name}&startDate={startDate}&endDate={endDate}")
//                        .uriVariable("name", "payload")
//                        .uriVariable("startDate", "headers.startDate")
//                        .uriVariable("endDate", "headers.endDate")
//                        .httpMethod(HttpMethod.GET)
//                        .expectedResponseType(String.class))
//                .transform(Scripts.processor("classpath:/JsonForms2HAL.js")
//                        .lang("javascript"))
//                .handle("customTransformer", "fromHALForms")
//                .channel("gather-channel")
//                .get();
//    }
//
//    @Bean
//    IntegrationFlow gatherComponent() {
//        return IntegrationFlows.from("gather-channel")
//                .aggregate(spec -> spec.outputProcessor(proc ->
//                                new CollectionModel<>(
//                                        proc.getMessages()
//                                                .stream()
//                                                .map(msg -> ((CollectionModel) msg.getPayload()).getContent())
//                                                .collect(Collectors.toList())))
////                        .groupTimeout(2000)
////                        .releaseStrategy(g -> g.size() > 1)
////                        .sendPartialResultOnExpiry(true)
//                )
//                .channel("rep-channel")
//                .get();
//    }
//}

@SpringBootApplication
public class DemoApplication {

    //All other interactions
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
        RentalService service = ctx.getBean(RentalService.class);
        System.out.println(
                service.findPlants("exc", LocalDate.now(), LocalDate.now().plusDays(2))
        );
    }
    //JSON to Java Object
//    public static void main(String[] args) {
//        ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);
//        RentalService service = ctx.getBean(RentalService.class);
//        System.out.println(
//                Arrays.asList(
//                        (Plant[])service.findPlants("exc", LocalDate.now(), LocalDate.now().plusDays(2))
//                )
//        );
//    }
}
