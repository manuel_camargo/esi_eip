var response = JSON.parse(payload);
JSON.stringify(
    response._embedded.plants.map(function (plant) {
        return {
            _id: plant._id,
            name: plant.name,
            description: plant.description,
            price: plant.price
        };
    })
);
